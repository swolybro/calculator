import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})

export class CalculatorComponent implements OnInit {

  public result: any;
  public value: any;
  public decimal: boolean;
  public total: Array<number>;
  public answer: number;
  public clear: boolean;
  public previous_operator: any;

  public constructor() {
    this.resetCalculator();
  }

  public ngOnInit() {

  }

  public addToCalculation(value) {

    if(this.clear == true || this.result == '0') {
      this.result = '';
      this.clear = false;
    }

    if(value == '.') {
      if(this.decimal == true) {
          return false;
      }
      this.decimal = true;
    }

    if(value == 'square') {
      this.result = this.result * 2;
      return false;
    }

    this.result += value;
  }

  public calculate(operator) {

    this.total.push(this.result);
    this.result = '';

    if(this.total.length == 2) {

      var a = Number(this.total[0]);
      var b = Number(this.total[1]);

      if(this.previous_operator == '+') {
          var total = a + b;    this.result = '';
          this.decimal = false;
          this.total = [];
          this.clear = false;
          this.previous_operator = false;''
      } else if(this.previous_operator == '-') {
          var total = a - b;
      } else if(this.previous_operator == '*') {
          var total = a * b;
      } else {
          var total = a / b;
      }

      var answer = total;

      this.total = [];
      this.total.push(answer);
      this.result = total;
      this.clear = true;
    }

    else {
      this.clear = false;
    }

    this.decimal = false;
    this.previous_operator = operator;

  }

  public getTotal() {

    var a = Number(this.total[0]);
    var b = Number(this.result);

    if(this.previous_operator == '+') {
        var total = a + b;
    } else if(this.previous_operator == '-') {
        var total = a - b;
    } else if(this.previous_operator == '*') {
        var total = a * b;
    } else {
        var total = a / b;
    }

    if(isNaN(total)) {
        return false;
    }

    this.result = total;
    this.total = [];
    this.clear = true;
  }

  public resetCalculator() {
    this.result = 0;
    this.decimal = false;
    this.total = [];
    this.clear = false;
    this.previous_operator = false;
  }

  public deleteLastNumber() {
    if(this.result > 0) {
      this.result = Math.floor(this.result/10);
    }
  }

}
