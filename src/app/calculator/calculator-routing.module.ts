import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { CalculatorComponent } from './components/calculator/calculator.component';
import { NotFoundComponent } from '../shared/components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    component: CalculatorComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalculatorRoutingModule { }
