import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { CalculatorRoutingModule } from './calculator-routing.module';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CalculatorRoutingModule,
    SharedModule
  ],
  declarations: [CalculatorComponent]
})
export class CalculatorModule { }
